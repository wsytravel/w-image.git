import WImage from "../packages/w-image";
//把组件保存在一个数组中
const components = [WImage]

//定义 install 方法
const install = function(Vue){
    // @ts-ignore
    if(install.installed) return
    // @ts-ignore
    install.installed = true
    components.map(component =>{
        Vue.component(component.name,component)
    })
}
// @ts-ignore
if(typeof window != 'undefined' && window.Vue){
    // @ts-ignore
    install(window.Vue)
}

export default {
    install,
    WImage
}
