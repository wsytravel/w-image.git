// @ts-ignore
import WImage from './src/main.vue'

WImage.install = function(Vue){
    Vue.component(WImage.name,WImage);
}

export default WImage;